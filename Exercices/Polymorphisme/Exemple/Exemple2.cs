﻿using System;

namespace PExemple2
{
    class Figure
    {
        public virtual void VoirDetails()
        {
            Console.WriteLine("<Classe de base> -> Figure");
        }
    }

    class Rectangle : Figure
    {
        public override void VoirDetails()
        {
            Console.WriteLine("<Classe dérivé> -> Rectangle");
        }
    }

}
