﻿using System;

namespace PExemple1
{
    class Figure
    {
        public void VoirDetails()
        {
            Console.WriteLine("<Classe de base> -> Figure");
        }
    }

    class Rectangle : Figure
    {
        public new void VoirDetails()
        {
            Console.WriteLine("<Classe dérivé> -> Rectangle");
        }
    }

}
