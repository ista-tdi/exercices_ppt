﻿using System;

namespace PExemple3
{
    public class Figure
    {

        public float a;
        public float b;

        public virtual void VoirDetails()
        {
            Console.WriteLine("<Classe de base> -> Figure");
        }

        public virtual void VoirMonReference()
        {
            Console.WriteLine("Figure");
        }


        public virtual float CalculeSurface()
        {

            return 0.0f;
        }

        public virtual float CalculePerimetre()
        {

            return 0.0f;
        }
    }

    public class Rectangle : Figure
    {

        // par defaut 
        // public float a;
        // public float b;

        public override void VoirDetails()
        {
            Console.WriteLine("<Classe dérivé> -> Rectangle");
        }

        public new void VoirMonReference()
        {
            Console.WriteLine("Rectangle");
        }


        public override float CalculeSurface()
        {
            return a* b;
        }

        public override float CalculePerimetre()
        {

            return 2 * (a + b);
        }
    }



    public class Cercle : Figure
    {

        // par defaut 
        // public float a;
        // public float b;

        public override void VoirDetails()
        {
            Console.WriteLine("<Classe dérivé> -> Rectangle");
        }

        public new void VoirMonReference()
        {
            Console.WriteLine("Rectangle");
        }


        public override float CalculeSurface()
        {
            return a * 3.14f;
        }

        public override float CalculePerimetre()
        {

            return a*a * 3.14f;
        }
    }



    public class Carre : Rectangle
    {
        public override void VoirDetails()
        {
            Console.WriteLine("<Classe dérivé> -> Carre");
        }

        public new void VoirMonReference()
        {
            Console.WriteLine("Carre");
        }
    }
}
