﻿using System;

namespace PExercice
{
    class Salary
    {
        private int matricule;
        private string categorie;
        private string service_;
        private string nom;
        private float salaire;

        private static int conteur = 0;

        public Salary() {
            conteur++;
        }

        public Salary(int matricule, string categorie, string service_, string nom, float salaire)
        {
            this.matricule = matricule;
            this.categorie = categorie;
            this.service_ = service_;
            this.nom = nom;
            this.salaire = salaire;

            conteur++;
        }

        public int Matricule
        {
            get { return matricule; }
            set { matricule = value;  }
        }

        public string Categorie
        {
            get { return categorie; }
            set { categorie = value; }
        }

        public string Service
        {
            get { return service_; }
            set { service_ = value; }
        }
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public float Salaire
        {
            get { return salaire; }
            set { salaire = value; }
        }


        public virtual void AfficherSalaire()
        {
            Console.WriteLine("Je suis {0}, Mon Salaire est {1}", nom, salaire);
        }

    }


    class Commercial : Salary
    {

        private float commission;

        public float Commision
        {
            get { return commission; }
            set { commission = value; }
        }



        // public Commercial() : base { } --> valide
        // public Commercial(){ } --> valide
        public Commercial() { }

        public Commercial(int commission, int matricule, string categorie, string service_, string nom, float salaire): 
            base(matricule, categorie, service_, nom, salaire + commission)
        {
            this.commission = commission;
        }


        public override void AfficherSalaire()
        {
            //base.AfficherSalaire();
            Console.WriteLine("Je suis le commercial {0} mon nouveau salaire est {1}", Nom, Salaire);
        }

        public void GotoClient() { }
    }
}
