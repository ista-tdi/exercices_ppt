﻿using System;

namespace M14SerieN3
{
    class Program
    {
        static void Main(string[] args)
        {
            Person patron = new Patron(120000, 1, "Mohamed", "IAM", 120137);

            Person cadre1 = new Cadre(1, "Ahmed", "INWI", 489732);
            Person cadre2 = new Cadre(2, "Karim", "ORANGE", 4322);


            Person ouvrier1 = new Ouvrier(9000, 100000, "Khalid", "ISTA", 3214);
            Person ouvrier2 = new Ouvrier(500, 100000, "Saad", "FST", 5432);
            Person ouvrier3 = new Ouvrier(5300, 100000, "Amine", "BTS", 1222);
            Person ouvrier4 = new Ouvrier(90000, 100000, "ISSAM", "ISTA", 7654);
            Person ouvrier5 = new Ouvrier(65000, 100000, "Badr", "ISTA", 98908);


            Console.WriteLine(patron.GetInfo());

            Console.WriteLine(cadre1.GetInfo());
            Console.WriteLine(cadre2.GetInfo());

            Console.WriteLine(ouvrier1.GetInfo());
            Console.WriteLine(ouvrier2.GetInfo());
            Console.WriteLine(ouvrier3.GetInfo());
            Console.WriteLine(ouvrier4.GetInfo());
            Console.WriteLine(ouvrier5.GetInfo());


        }
    }



    public class Person
    {
        private string nom;
        private string entreprise;
        private int numSecu;

        // Constructeur sans parametre
        // new Person()
        public Person() { }

        // Constructeur avec un seul parametre/argument
        // new Person("Mohamed")
        public Person(string nom)
        {
            this.nom = nom;
        }

        // Constructeur avec 2 parametres/arguments
        // new Person("Mohamed", "ISTA")
        public Person(string nom, string entreprise) : this(nom)
        {
            this.entreprise = entreprise;
        }

        // Constructeur avec 3 parametres/arguments
        // new Person("Mohamed", "ISTA", 2003)
        public Person(string nom, string entreprise, int numSecu) : this(nom, entreprise)
        {
            this.numSecu = numSecu;
        }


        public string Nom
        {
            // Getter
            // string n = p.Nom;
            get { return nom; }

            // Setter
            // p.Nom = "Mohamed"
            set { nom = value; }
        }

        // ##############################
        // ###### NOTE ##################
        // ##############################
        /*
         
            On peut remplacer le getter et le setter de l'attribute 'nom' par celui là 
        

            // Getter
            // n = p.GetNom();
           public string GetNom()
           {
                return nom;
           }

            // Setter
            // p.SetNom("Mohamed");
           public void SetNom(string nom)
           {
               this.nom = nom;
           } 


         */

        public string Entreprise
        {
            get { return entreprise; }
            set { entreprise = value; }
        }

        public int NumSecu
        {
            get { return numSecu; }
            set { numSecu = value; }
        }

        public virtual float GetSalaire()
        {
            return 0;
        }

        // Virtual est obligatoir pour faire ovveride dans d'autre classe
        public virtual string GetInfo()
        {
            return String.Format("Nom: {0}, Entreprise: {1}, NumSecu: {2}", nom, entreprise, numSecu);
        }

    }


    // Les classes commence par un Majiscule
    public class Ouvrier : Person
    {


        // LES ATTRIBUTES SONT 'private' ET miniscule
        // ; à la fin
        private int dateEntree;
        private int dateCourante;
        private int age;

        // const pour créer une variable constante
        // Les constants sont tous MAJISCULE
        private const float SMIG = 1000.00f;


        public int DateEntree
        {
            // ; à la fin
            get { return dateEntree; }
            set { dateEntree = value; }
        }

        public int DateCourante
        {
            get { return dateCourante; }
            set { dateCourante = value; }
        }

        public Ouvrier() { }

        public Ouvrier(int dateEntree, int dateCourante, string nom) : base(nom)
        {
            this.dateCourante = dateCourante;
            this.dateEntree = dateEntree;
        }

        public Ouvrier(int dateEntree, int dateCourante, string nom, string entreprise) : base(nom, entreprise)
        {
            this.dateCourante = dateCourante;
            this.dateEntree = dateEntree;
        }

        // base pour utiliser le constructeur de base (Person dans ce cas)
        public Ouvrier(int dateEntree, int dateCourante, string nom, string entreprise, int numSecu) :  base(nom, entreprise, numSecu)
        {
            this.dateCourante = dateCourante;
            this.dateEntree = dateEntree;
        }


        public int Age
        {
            get { return age; }
            set { age = value; }
        }


        // override est obligatoir pour utiiliser GetSalaire de Person
        public override float GetSalaire()
        {
            float newSalaire = SMIG + (age - 18) * 100 + (dateCourante - dateEntree) * 150;
            if (newSalaire > SMIG * 2)
                return SMIG * 2;

            return newSalaire;
        }

        public override string GetInfo()
        {
            return base.GetInfo() + String.Format(" Salaire: {0}", GetSalaire());
        }
    }


    public class Cadre : Person
    {
        private int indice;

        public int Indice
        {
            get { return indice; }
            set { indice = value; }
        }

        public Cadre() { }

        public Cadre(int indice, string nom) : base(nom)
        {
            this.indice = indice;
        }

        public Cadre(int indice, string nom, string entreprise) : base(nom, entreprise)
        {
            this.indice = indice;
        }

        public Cadre(int indice, string nom, string entreprise, int numSecu) :
            base(nom, entreprise, numSecu)
        {
            this.indice = indice;
        }


        public override float GetSalaire()
        {
            if (indice == 1)
                return 130000;
            else if (indice == 2)
                return 150000;
            else if (indice == 3)
                return 170000;
            else if (indice == 4)
                return 200000;
            else
                return 0;
        }

        public override string GetInfo()
        {
            return base.GetInfo() + String.Format(" Salaire: {0}", GetSalaire());
        }
    }

    public class Patron : Person
    {

        private int ca;
        private int pourcentage;

        public Patron() { }

        public Patron(int ca, int pourcentage, string nom) : base(nom)
        {
            this.ca = ca;
            this.pourcentage = pourcentage;
        }

        public Patron(int ca, int pourcentage, string nom, string entreprise) : base(nom, entreprise)
        {
            this.ca = ca;
            this.pourcentage = pourcentage;
        }

        public Patron(int ca, int pourcentage, string nom, string entreprise, int numSecu) : base(nom, entreprise, numSecu)
        {
            this.ca = ca;
            this.pourcentage = pourcentage;
        }


        public int CA
        {
            get { return ca; }
            set { ca = value; }
        }

        public int Pourcentage
        {
            get { return pourcentage; }
            set { pourcentage = value; }
        }


        public override float GetSalaire()
        {
            return (ca * pourcentage) / 100;
        }

        public override string GetInfo()
        {
            return base.GetInfo() + String.Format(" Salaire: {0}", GetSalaire());
        }
    }



}
