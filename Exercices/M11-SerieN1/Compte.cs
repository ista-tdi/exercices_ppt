﻿using System;

namespace Exercices.M11_SerieN1
{
    class Compte
    {

       /* const char nbLiquide = 'L';
        const char nbCB = 'C';
        const char nbCheque = 'Q';*/

        public enum TypeOperation { L, C, Q }

        private int nbLiquide;
        private int nbCB;
        private int nbCheque;

        public int NbLiquide { get { return nbLiquide; } }
        public int NbCB { get { return nbCB; } }
        public int NbCheque { get { return nbCheque; } }

        // private float solde;
        public float Solde {get; set;}

        public string Code { get; set;}

        public float SommeDepots { get; set; }

        public float SommeRetraits { get;set; }

        private int decouvert;
        private float tauxInteret;

        public int Decouvert {
            get { return decouvert;  }
        }

        public float TauxInteret
        {
            get { return tauxInteret; }
        }

        public Compte()
        {
            Solde = 0;
        }


        public Compte(string code, float solde)
        {
            // setSolde(solde)
            Solde = solde;
            Code = code;
        }


        public void Depot(string code, float somme)
        {
            if (Code.Equals(code) && somme > 0)
            {
                Solde += somme;
                SommeDepots += somme;
                Console.WriteLine(">> La somme {0} est déposé sur le compte {1}", somme, Code);
                Console.WriteLine(">> La somme total des dépots est: {0}", SommeDepots);
            }
            else
            {
                Console.WriteLine(">> La somme {0} est n'est pas déposé ducompte {1}", somme, Code);
            }
        }


        // Etape 5
        public void Depot(TypeOperation operation, string code, float somme)
        {
            if (Code.Equals(code) && somme > 0)
            {
                Solde += somme;
                SommeDepots += somme;
                Console.WriteLine(">> La somme {0} est déposé sur le compte {1} --> Par {2}", somme, Code, operation.ToString());
                Console.WriteLine(">> La somme total des dépots est: {0}", SommeDepots);

                switch (operation)
                {
                    case TypeOperation.C: nbCB++; break;
                    case TypeOperation.Q: nbCheque++; break;
                    default: nbLiquide++; break;
                }
            }
            else
            {
                Console.WriteLine(">> La somme {0} est n'est pas déposé ducompte {1}", somme, Code);
            }
        }


        public void Retrait(string code, float somme)
        {
            if (Code.Equals(code) && somme > 0)
            {
                if ((SommeRetraits + somme) > Decouvert && Decouvert != 0)
                {
                    Console.WriteLine("#### Vous n'êtes pas autoriser");
                }
                else
                {
                    Solde -= somme;
                    SommeRetraits += somme;
                    Console.WriteLine(">> La somme {0} est retiré sur le compte {1}", somme, Code);
                    Console.WriteLine(">> La somme total des retraite est: {0}", SommeRetraits);
                }
            }
            else
            {
                Console.WriteLine(">> La somme {0} est n'est pas retiré du compte {1}", somme, Code);
            }
            
        }


        // Pour Étape 5
        public void Retrait(TypeOperation operation, string code, float somme)
        {
            if (Code.Equals(code) && somme > 0)
            {
                if ((SommeRetraits + somme) > Decouvert  && Decouvert != 0)
                {
                    Console.WriteLine("#### Vous n'êtes pas autoriser");
                }

                else
                {

                    switch (operation)
                    {
                        case TypeOperation.C: nbCB++; break;
                        case TypeOperation.Q: nbCheque++; break;
                        default: nbLiquide++; break;
                    }


                    Solde -= somme;
                    SommeRetraits += somme;
                    Console.WriteLine(">> La somme {0} est retiré sur le compte {1} ---> Par: {2}", somme, Code, operation.ToString());
                    Console.WriteLine(">> La somme total des retraite est: {0}", SommeRetraits);
                    
                    
                    /*
                    --------  Switch c'est equivalent de --------

                    if (TypeOperation.C.Equals(operation))
                    {
                        nbCB++;
                    }
                    else if (TypeOperation.Q.Equals(operation))
                    {
                        nbCheque++;
                    }
                    else
                    {
                        nbLiquide++;
                    }
                    
                     */

                }
            }
            else
            {
                Console.WriteLine(">> La somme {0} est n'est pas retiré du compte {1}", somme, Code);
            }

        }


        public void AfficherSolde()
        {
            Console.WriteLine("Mon Solde est {0} DHs", Solde);
        }

        public void InsererDecouvert()
        {
            Console.Write("Inserer le découvert: ");
            decouvert = int.Parse(Console.ReadLine());
            // Decouvert = Convert.ToInt32(Console.ReadLine());
        }

        public void SimulerTauxInterer()
        {
            Console.Write("Inserer le taux d'interet: ");
            tauxInteret = float.Parse(Console.ReadLine());

            Console.WriteLine("Mon solde apres la 1er Annee {0}", MonSoldeParAnnee(Solde));
            Console.WriteLine("Il me faut {0} année pour avoir le double de {1}", CalculeAnneeDoubleSolde(), Solde);

        }


        private float CalculeAnneeDoubleSolde()
        {
            float newSolde = Solde;
            int annee = 0;
            // solde: 100 - annee = 0
            // ...
            // solde: 200 - annee = X
            
            while(newSolde < Solde * 2)
            {
                newSolde = MonSoldeParAnnee(newSolde);
                annee++;
            }

            return annee;
        }

        private float MonSoldeParAnnee(float newSolde)
        {
            // solde = 100 , interer 1%
            // 1: 100 + (100 * 0.01) = 101
            // 2: 101 + (101 * 0.01) ~ 102

            // Mon solde dans 12 mois
            for (int i = 0; i < 12; i++)
            {
                newSolde += newSolde * TauxInteret;
            }

            return newSolde;
        }

    }
}
