﻿using System;

namespace Exercice4
{
    class Produit
    {

        private string nom;
        private float prixAchat;
        private float prixVente;
        private int exemplaire;
        private string description;

        public string Nom{
            get { return nom; }
            set { this.nom = value;}
        }

        public float PrixAchat
        {
            get { return prixAchat;  }
            set { prixAchat = value;  }
        }

        public float PrixVente
        {
            get { return prixVente;  }
            set { prixVente = value;  }
        }

        public int Exemplaire
        {
            get { return exemplaire;  }
            set { exemplaire = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }



        public void Afficher()
        {
            Console.WriteLine(" Nom: {0} -- PrixAchat: {1} --  PrixVente: {2} --  Exemplaire: {3} --  Description: {4} -- ",
                nom, prixAchat, prixVente, exemplaire, description);
        }
    }
}
