﻿using System;

namespace Exercice3
{
    class Eleve
    {

        // Attribue Global
        private int code;
        private string nom;
        private DateTime date;

        public int GetCode()
        {
            return code;
        }


        public void SetCode(// variable local à la methode SetCode
                            int code){
            this.code = code;

        }


        public string GetNom()
        {
            // on peut utiliser "this" n'import où dans la classe
            return this.nom;
        }

        public void SetNom(string name)
        {
            // On a pas besoin d'utiliser "this" parceque nom & name sont different (nommination)
            nom = name;
        }


        public DateTime GetDate()
        {
            return date;
        }


        public void SetDate(DateTime date)
        {
            this.date = date;
        }



        public void Afficher()
        {
            Console.WriteLine("Code: {0}  -- Nom: {1} -- Date: {2}", code, nom, date);
        }

    }
}
