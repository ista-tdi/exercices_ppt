﻿using System;

namespace Exercices1
{
    class Salarie
    {
        // Attributes
        /*
         * Commentaire
         */
        public int matricule;
        public string status;
        public string service;
        public string nom;
        public float salaire;


        public void Afficher()
        { 

            // Exemple pour afficher une valeur -> en utilisant {0}
            Console.WriteLine("Matricule: {0}", matricule);
            Console.WriteLine("Status: {0}", status);
            Console.WriteLine("Service: {0}", service);

            // Exemple pour afficher une valeur -> en utilisant '+'
            Console.WriteLine("Nom: "+ nom);
            Console.WriteLine("Salaire: "+ salaire +"\n\n" ); // convert salaire (float) vers String
        }


    }
}
