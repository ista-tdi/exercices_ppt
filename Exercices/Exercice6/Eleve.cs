﻿using System;

namespace Exercices.Exercice6
{
    class Eleve
    {
        // Attribue Global
        private int code;
        private string nom;
        private string prenom;
        private DateTime date;


        // Contructeur qui initialise le code & le nom
        public Eleve(int code, string nom)
        {
            this.code = code;
            this.nom = nom;
        }


        // Constructeur qui initialise tous les attributes
        // + utiliser le constructeur au dessus
        public Eleve(int code, string nom, string prenom, DateTime date): this(code, nom)
        {
            this.prenom = prenom;
            this.date = date;
        }


        // Constructeur de recopie
        public Eleve(Eleve eleve)
        {
            this.code = eleve.code;
            this.nom = eleve.nom;
            this.prenom = eleve.prenom;
            this.date = eleve.date;
            this.code = eleve.code;
            this.nom = eleve.nom;
        }



        public int Code
        {
            get { return code; }
            set { code = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }


        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public void Afficher()
        {
            Console.WriteLine("Code: {0}  -- Nom: {1} -- Date: {2}", code, nom, date);
        }
    }
}
