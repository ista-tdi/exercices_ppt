﻿using System;

namespace Exercices2
{
    class Module
    {
        public string reference;
        public string intitule;


        public void Afficher()
        {
            Console.WriteLine("Réference: {0}  ---  Intitulé: {1}", reference, intitule);
            Console.WriteLine();
        }
    }

    class Formateur
    {
        public int matricule;
        public string nom;
        public string prenom;
        public DateTime date;

        public void Afficher()
        {
            Console.WriteLine("Matricule: {0}  ---  Nom: {1}  --- Prenom: {2}  --- Date: {3}", 
                matricule, nom, prenom, date);
            Console.WriteLine();
        }
    }


}
