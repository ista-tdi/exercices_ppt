﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercices.Static
{

    /*
    *                    0                        0
                        STATIC                  NON-STATIC
    
ETAPE1      Person1 ->   1                          1
            Person2 ->   1                          0
            Person3 ->   1                          0

ETAPE2      Person1 ->   2                          1
            Person2 ->   2                          2
            Person3 ->   2                          0

ETAPE3      Person1 ->   3                          1
            Person2 ->   3                          2
            Person3 ->   3                          3


    

     */

    class Person
    {
        private string nom;
        public static int conteur;
        private int nonStaticConteur;

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public int Conteur
        {
            get { return conteur; }
            set { conteur = value; }
        }


        public int NonStaticConteur
        {
            get { return nonStaticConteur; }
            set { nonStaticConteur = value; }
        }
    }
}
