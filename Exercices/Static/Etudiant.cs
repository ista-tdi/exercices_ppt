﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
    - Le nombre des etidiants créer
    - Numéro est unique:
        Etudiant_1 = -> 1
        Etudiant_2 = -> 2
        ....
        Etudiant_n = -> n

**/


namespace Exercices.Static
{
    class Group
    {

        // Total de tout les etudiants ajouter (dans tout les groups)
        public static int nombreTotalEtudiants = 0;
        public int nombreEtudiantsParGroup = 0;


        public int NombreEtudiantsParGroup
        {
            get { return nombreEtudiantsParGroup; }
            set { nombreEtudiantsParGroup = value; }
        }

        public string Nom
        {
            get;
            set;
        }

        public void AjouterEtudiant()
        {
            nombreTotalEtudiants++;
            nombreEtudiantsParGroup++;
        }

        public void SupprimerEtudiant()
        {
            nombreTotalEtudiants--;
            nombreEtudiantsParGroup--;
        }
    }

    public class Vehicule
    {
        private int poids;

        public Vehicule(int poids)
        {
            this.poids = poids;
        }

        public string Description()
        {
            return "Véhicule de " + poids + " tonnes";
        }
    }






    public class Automobile : Vehicule
    {
        private string couleur;

        public Automobile(int poids, string couleur) : base(poids)
        {
            this.couleur = couleur;
        }

        // méthode redéfinie
        public new string Description()
        {
            return base.Description() + " de couleur " + couleur;
        }
    }
}
