﻿using System;
using Heritage;

namespace Heritage
{
    class Enseignant : Person
    {
        private string section;


        public Enseignant() { }

        public Enseignant(string nom, string prenom, string section) : base(nom, prenom)
        {
            this.section = section;
        }


        public Enseignant(string nom, string prenom, int age, string section): base(nom,prenom, age)
        {
            this.section = section;
        }


        public string Section
        {
            get { return section; }
            set { section = value; }
        }


        public void AfficherMonDetail()
        {
            Console.WriteLine("Enseignant: >>> " + base.Afficher() + " Section: " + section);
        }

        public override bool Equals(Object obj)
        {
            Enseignant en = (Enseignant)obj;
            return (en.Section == this.section) && (en.Age == this.Age);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
