﻿using System;

namespace Heritage
{
    class Person
    {
        private string nom;
        private string prenom;
        private int age;

        public Person() { }

        public Person(string nom, string prenom)
        {
            this.nom = nom;
            this.prenom = prenom;
        }


        public Person(string nom, string prenom, int age): this(nom, prenom)
        {
            this.age = age;
        }



        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }



        public string Afficher()
        {
            return "Person -> Nom: "+nom+", Prenom: "+prenom+", Age: "+age;
        }


        private void TestPrivate() { }

        protected void TestProtected() { }
    }
}
