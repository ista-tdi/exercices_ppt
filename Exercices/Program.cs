﻿using System;
using Exercices1;
using Exercices2;
using Exercice3;
using Exercices.Static;
using PExemple1;
using Exercices.M11_SerieN1;
using PExercice;

namespace Exercices
{

    class Program
    {
       
        // Main 
        static void Main(string[] args)
        {

            Console.WriteLine("A: ");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("B: ");
            int b = int.Parse(Console.ReadLine());

            try
            {
                float result = a / b;
                Console.WriteLine(result);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("<<<< EROOOR >>> {0}", e.Message);
            }

            /* try
             {
                 if (b == 0)
                 {
                     throw new ArgumentOutOfRangeException();
                 }
             }
             catch (ArgumentOutOfRangeException aor)
             {
                 Console.WriteLine("<<<< EROOOR1 >>> {0}", aor.Message);
             }
             try
             {
                 float result = a / b;
                 Console.WriteLine(result);
             }
             catch (DivideByZeroException e)
             {
                 Console.WriteLine("A: ");
                 a = int.Parse(Console.ReadLine());
                 Console.WriteLine("<<<< EROOOR >>> {0}", e.Message);
             }
             finally
             {
                 Console.WriteLine("Final Programme");
             }


             // Traitement ......
             Console.WriteLine("Traitement ......");*/


        }

        public static void MainException()
        {

            Console.WriteLine("A: ");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("B: ");
            int b = int.Parse(Console.ReadLine());

            try
            {
                if (b == 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            catch (ArgumentOutOfRangeException aor)
            {
                Console.WriteLine("<<<< EROOOR1 >>> {0}", aor.Message);
            }
            try
            {
                float result = a / b;
                Console.WriteLine(result);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("A: ");
                a = int.Parse(Console.ReadLine());
                Console.WriteLine("<<<< EROOOR >>> {0}", e.Message);
            }
            finally
            {
                Console.WriteLine("Final Programme");
            }


            // Traitement ......
            Console.WriteLine("Traitement ......");
        }

        public static void Abstract()
        {
            Abstract.Figure rectangle = new Abstract.Rectangle(10, 5);
            Abstract.Figure cercle = new Abstract.Cercle(10);

            double rPerimetre = rectangle.CalculePerimetre();
            double rSurface = rectangle.CalculeSurface();
            Console.WriteLine("Le périmetre du Rectangle {0}x{1} = {2}",
                ((Abstract.Rectangle)rectangle).Largeur, ((Abstract.Rectangle)rectangle).Longueur, rPerimetre);
            Console.WriteLine("La surface du Rectangle {0}x{1} = {2}",
                ((Abstract.Rectangle)rectangle).Largeur, ((Abstract.Rectangle)rectangle).Longueur, rSurface);

            double cPerimetre = cercle.CalculePerimetre();
            double cSurface = cercle.CalculeSurface();
            Console.WriteLine("Le Perimetre du Cercle avec le rayon {0} est {1}",
                ((Abstract.Cercle)cercle).Rayon, cPerimetre);
            Console.WriteLine("La surface du Cercle avec le rayon {0} est {1}",
                ((Abstract.Cercle)cercle).Rayon, cSurface);
        }


        public static void M11_Etape5()
        {
            Compte compte2 = new Compte("BB1000000", 10000);
            Console.WriteLine();
            Console.WriteLine("Les operateurs> Cheque {0}, Carte {1}, Liquide {2}", compte2.NbCheque, compte2.NbCB, compte2.NbLiquide);
            compte2.Retrait(Compte.TypeOperation.L, "BB1000000", 1000);

            Console.WriteLine();
            Console.WriteLine("Les operateurs> Cheque {0}, Carte {1}, Liquide {2}", compte2.NbCheque, compte2.NbCB, compte2.NbLiquide);
            compte2.Retrait(Compte.TypeOperation.C, "BB1000000", 200);

            Console.WriteLine();
            Console.WriteLine("Les operateurs> Cheque {0}, Carte {1}, Liquide {2}", compte2.NbCheque, compte2.NbCB, compte2.NbLiquide);
            compte2.Retrait(Compte.TypeOperation.Q, "BB1000000", 300);

            Console.WriteLine();
            Console.WriteLine("Les operateurs> Cheque {0}, Carte {1}, Liquide {2}", compte2.NbCheque, compte2.NbCB, compte2.NbLiquide);
            compte2.Depot(Compte.TypeOperation.L, "BB1000000", 2000);

            Console.WriteLine();
            Console.WriteLine("Les operateurs> Cheque {0}, Carte {1}, Liquide {2}", compte2.NbCheque, compte2.NbCB, compte2.NbLiquide);
            compte2.Depot(Compte.TypeOperation.C, "BB1000000", 100);

            Console.WriteLine();
            Console.WriteLine("Les operateurs> Cheque {0}, Carte {1}, Liquide {2}", compte2.NbCheque, compte2.NbCB, compte2.NbLiquide);
            compte2.Depot(Compte.TypeOperation.L, "BB1000000", 2000);

            Console.WriteLine();
            Console.WriteLine("Les operateurs> Cheque {0}, Carte {1}, Liquide {2}", compte2.NbCheque, compte2.NbCB, compte2.NbLiquide);
            compte2.Depot(Compte.TypeOperation.Q, "BB1000000", 300);

            Console.WriteLine();
            Console.WriteLine("Les operateurs> Cheque {0}, Carte {1}, Liquide {2}", compte2.NbCheque, compte2.NbCB, compte2.NbLiquide);
            compte2.Retrait(Compte.TypeOperation.Q, "BB1000000", 3000);

        }
        public static void M11_Etape4()
        {
            Compte compte2 = new Compte("BB1000000", 100);
            compte2.SimulerTauxInterer();
        }



            public static void M11_Etape1() {
            Compte compte = new Compte("AA1000000", 100);
            compte.AfficherSolde();


            // Code valide
            compte.Depot("AA1000000", 1000);
            compte.AfficherSolde();
            compte.Depot("AA1000000", 200);
            compte.AfficherSolde();
            compte.Depot("AA1000000", 300);
            compte.AfficherSolde();

            // Code erroné
            compte.Depot("BBBB22000", 1000);
            compte.AfficherSolde();


            compte.InsererDecouvert();
            compte.Retrait("AA1000000", 100);
            compte.AfficherSolde();
            compte.Retrait("AA1000000", 400);
            compte.AfficherSolde();
            compte.Retrait("PBDY@EWDs", 100);
            compte.AfficherSolde();
        }


        // Ce n'est un Main
        // C'est just une simple methode avec le nom Main...
        public static void MainExercice1()
        {
            // #########################
            // Création d'une nouvelle in
            // Version 1.0
            /*
             Si on a pas utilisé le "using Exercice2" on doit ajouter "Exercice2." pour accéder aux données du namespace
             EX: Exercice2.Salarie salary = new Exercice2.Salarie();
            */
            Salarie salary = new Salarie();
            salary.matricule = 100;
            salary.status = "Directeur";
            salary.service = "Informatique";
            salary.nom = "Mohamed";

            // salaire de type float
            // Les valeurs valide sont:
            /* <<EXEMPLE>>
             * 1000
             * 1000f
             * 1000F
             * 1000.1f
             * 1000.1F
             */
            salary.salaire = 1000;

            salary.Afficher();

            // Je travail encore avec le meme objet "salary"
            // Version 1.1
            salary.matricule = 101;
            salary.status = "Directeur";
            salary.service = "Informatique";
            salary.nom = "Mohamed";
            salary.salaire = 1002.00f;

            salary.Afficher();




            // #########################
            // Création d'une nouvelle instance == nouveau objet
            // Version 2.0
            Salarie salary2 = new Salarie();
            salary2.matricule = 2000;
            salary2.status = "Coursier";
            salary2.service = "Informatique";
            salary2.nom = "Khalid";
            salary2.salaire = 3000.00f;

            // il affiche le contenu de 2.0
            salary2.Afficher();



            // il affiche le contenu de 1.1
            salary.Afficher();

            // il affiche le contenu de 2.0
            salary2.Afficher();
        }

        public static void MainExercice2() {
            // Creation d'une instance vide de type Formateur
            Formateur formateur = new Formateur();
            Module module = new Module();

            formateur.nom = "SALHI";
            formateur.prenom = "Mohamed";
            formateur.date = DateTime.Now;
            formateur.matricule = 1029634;

            module.intitule = "Orienté Objet";
            module.reference = "A129717";


            formateur.Afficher();
            module.Afficher();
        }
    
        public static void MainExercice3()
        {
            Eleve eleve = new Eleve();

            eleve.Afficher();
            Console.WriteLine("Code: {0}  -- Nom: {1} -- Date: {2}", eleve.GetCode(), eleve.GetNom(), eleve.GetDate());

            eleve.SetCode(1000);
            eleve.SetNom("Mohamed");
            eleve.SetDate(DateTime.Now);

            Console.WriteLine();
            eleve.Afficher();
            Console.WriteLine("Code: {0}  -- Nom: {1} -- Date: {2}", eleve.GetCode(), eleve.GetNom(), eleve.GetDate());
        }

        public static void MainExercice4()
        {
            Exercice4.Produit produit = new Exercice4.Produit();
            produit.Nom = "Camera";
            produit.PrixAchat = 100.00f;
            produit.PrixVente = 2000;
            produit.Exemplaire = 10;
            produit.Description = "Camera Pro";

            produit.Afficher();
        }
 
        public static void ExempleStatic()
        {
            Person.conteur = 5;


            Person person1 = new Person();
            person1.Nom = "Mohamed";
            person1.NonStaticConteur = 5;


            Console.WriteLine("P1.Conteur: " + person1.Conteur);
            Console.WriteLine("P1.Non-Static.Conteur: " + person1.NonStaticConteur);

            person1.Conteur = 6;
            person1.NonStaticConteur = 6;

            Console.WriteLine("P1.Conteur: " + person1.Conteur);
            Console.WriteLine("P1.Non-Static.Conteur: " + person1.NonStaticConteur);

            Person person2 = new Person();

            person2.Nom = "Khalid";
            person2.Conteur = 7;
            person2.NonStaticConteur = 7;


            Console.WriteLine("P2.Conteur: " + person2.Conteur);
            Console.WriteLine("P2.Non-Static.Conteur: " + person2.NonStaticConteur);
            Console.WriteLine("P1.Conteur: " + person1.Conteur);
            Console.WriteLine("P1.Non-Static.Conteur: " + person1.NonStaticConteur);
        }

        public static void ExempleStatic2()
        {
            Group microsoftTeams = new Group();
            microsoftTeams.Nom = "microsoftTeams";

            Group skype = new Group();
            skype.Nom = "Skype";

            Group whatssap = new Group();
            whatssap.Nom = "whatssap";

            Group zoom = new Group();
            zoom.Nom = "zoom";

            // Ajouter le 1er etudiant dans le Group microsoftTeams
            microsoftTeams.AjouterEtudiant();

            Console.WriteLine("Total des etudiants dans tous les groups: {0}",
                Group.nombreTotalEtudiants);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                microsoftTeams.Nom, microsoftTeams.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                skype.Nom, skype.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                whatssap.Nom, whatssap.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                zoom.Nom, zoom.nombreEtudiantsParGroup);
            Console.WriteLine();

            // Ajouter le 2er etudiant dans le Group microsoftTeams
            microsoftTeams.AjouterEtudiant();

            Console.WriteLine("Total des etudiants dans tous les groups: {0}",
                Group.nombreTotalEtudiants);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                microsoftTeams.Nom, microsoftTeams.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                skype.Nom, skype.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                whatssap.Nom, whatssap.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                zoom.Nom, zoom.nombreEtudiantsParGroup);
            Console.WriteLine();



            // Ajouter le 1er etudiant dans le Group Zoom
            zoom.AjouterEtudiant();

            Console.WriteLine("Total des etudiants dans tous les groups: {0}",
                Group.nombreTotalEtudiants);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                microsoftTeams.Nom, microsoftTeams.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                skype.Nom, skype.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                whatssap.Nom, whatssap.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                zoom.Nom, zoom.nombreEtudiantsParGroup);
            Console.WriteLine();



            // Ajouter le 1er etudiant dans le Group Whatssap
            whatssap.AjouterEtudiant();

            Console.WriteLine("Total des etudiants dans tous les groups: {0}",
                 Group.nombreTotalEtudiants);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                microsoftTeams.Nom, microsoftTeams.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                skype.Nom, skype.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                whatssap.Nom, whatssap.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                zoom.Nom, zoom.nombreEtudiantsParGroup);
            Console.WriteLine();


            // Ajouter un etudiant dans le Group Zoom
            zoom.AjouterEtudiant();

            Console.WriteLine("Total des etudiants dans tous les groups: {0}",
                Group.nombreTotalEtudiants);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                microsoftTeams.Nom, microsoftTeams.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                skype.Nom, skype.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                whatssap.Nom, whatssap.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                zoom.Nom, zoom.nombreEtudiantsParGroup);
            Console.WriteLine();



            // Ajouter le 1er etudiant dans le Group Whatssap
            microsoftTeams.AjouterEtudiant();

            Console.WriteLine("Total des etudiants dans tous les groups: {0}",
                Group.nombreTotalEtudiants);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                microsoftTeams.Nom, microsoftTeams.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                skype.Nom, skype.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                whatssap.Nom, whatssap.nombreEtudiantsParGroup);
            Console.WriteLine("Total des etudiants dans le group {0}: {1}",
                zoom.Nom, zoom.nombreEtudiantsParGroup);
            Console.WriteLine();
        }

        public static void MainExercice5()
        {
            Exercice5.Produit produit1 = new Exercice5.Produit("Camera", 500, 1000, 10, "Camera Test");
            Exercice5.Produit produit2 = new Exercice5.Produit(produit1);

            Console.WriteLine("Creation des deux produits:");
            produit1.Afficher();
            produit2.Afficher();
            Console.WriteLine();

            produit2.PrixVente = 1200;
            produit2.Description = "Camera Pro";

            Console.WriteLine("Modification du 2eme produit:");
            produit1.Afficher();
            produit2.Afficher();
            Console.WriteLine();

            produit2 = produit1;

            Console.WriteLine("Copie par refernce de P1 -> P2:");
            produit1.Afficher();
            produit2.Afficher();
            Console.WriteLine();


            produit2.PrixVente = 1200;
            produit2.Description = "Camera Pro";

            Console.WriteLine("Modification du P2 avec une copie par refernce:");
            produit1.Afficher();
            produit2.Afficher();
            Console.WriteLine();
        }

        public void MainHeritage()
        {
            Heritage.Person person1 = new Heritage.Person();
            person1.Nom = "Khalid";
            person1.Prenom = "Karam";
            Console.WriteLine(person1.Afficher() + "\n");

            Heritage.Person person2 = new Heritage.Person("Ayman", "AYMANI");
            person2.Age = 22;
            Console.WriteLine(person2.Afficher() + "\n");

            Heritage.Person person3 = new Heritage.Person("SALHI", "Mohamed", 20);
            Console.WriteLine(person3.Afficher() + "\n");

                Heritage.Enseignant enseignant = new Heritage.Enseignant();
            enseignant.Nom = "Mouad";
            enseignant.Prenom = "Mouadi";
            enseignant.Age = 30;
            
            
            


            // Fils --> Parent
            // A b C -> A, B
            Heritage.Person enseignant2 = new Heritage.Enseignant();

            // Parent xxx> Fils
            // A B       > A B ...........................
            // Ce n' est pas possible
            //Heritage.Enseignant person = new Heritage.Person();
        }



        public static void MainPolymorpgisme()
        {
            // ####################################
            // EXEMPLE 1
            // ####################################
            Console.WriteLine("\n\n Exemple 1");

            Figure figure = new Figure();
            Console.WriteLine("Figure = new Figure");
            figure.VoirDetails(); // Figure


            Console.WriteLine("");
            Rectangle rectangle = new Rectangle();
            Console.WriteLine("Rectangle = new Rectangle");
            rectangle.VoirDetails(); // Rectangle


            Console.WriteLine("");
            // Tous les sous-classes de "FIGURES" sont accéptés comme instance pour le type "Figure"
            Figure figureRectangle = new Rectangle();
            Console.WriteLine("Figure = new Rectangle");
            figureRectangle.VoirDetails(); // Figure


            // ####################################
            // EXEMPLE 2
            // ####################################
            Console.WriteLine("\n\n Exemple 2");


            PExemple2.Figure figure2 = new PExemple2.Figure();
            Console.WriteLine("Figure = new Figure");
            figure2.VoirDetails(); // Figure


            Console.WriteLine("");
            PExemple2.Rectangle rectangle2 = new PExemple2.Rectangle();
            Console.WriteLine("Rectangle = new Rectangle");
            rectangle2.VoirDetails(); // Rectangle


            Console.WriteLine("");
            // Tous les sous-classes de "FIGURES" sont accéptés comme instance pour le type "Figure"
            PExemple2.Figure figureRectangle2 = new PExemple2.Rectangle();
            Console.WriteLine("Figure = new Rectangle");
            figureRectangle2.VoirDetails(); // Figure


            // ####################################
            // EXEMPLE 3
            // ####################################
            Console.WriteLine("\n\n Exemple 3");

            PExemple3.Figure figure3 = new PExemple3.Figure();
            Console.WriteLine("Figure = new Figure");
            figure3.VoirDetails(); // Figure
            figure3.VoirMonReference();


            Console.WriteLine("");
            PExemple3.Rectangle rectangle3 = new PExemple3.Rectangle();
            Console.WriteLine("Rectangle = new Rectangle");
            rectangle3.VoirDetails(); // Rectangle
            rectangle3.VoirMonReference();



            Console.WriteLine("");
            PExemple3.Carre carre3 = new PExemple3.Carre();
            Console.WriteLine("Carre = new Carre");
            carre3.VoirDetails(); // Figure
            carre3.VoirMonReference();





            Console.WriteLine("");
            // Tous les sous-classes de "FIGURES" sont accéptés comme instance pour le type "Figure"
            PExemple3.Figure figureRectangle3 = new PExemple3.Rectangle();
            Console.WriteLine("Figure = new Rectangle");
            figureRectangle3.VoirDetails(); // Figure
            figureRectangle3.VoirMonReference();

            Console.WriteLine("");
            PExemple3.Figure figureCarre3 = new PExemple3.Carre();
            Console.WriteLine("Figure = new Carre");
            figureCarre3.VoirDetails(); // Figure
            figureCarre3.VoirMonReference();





            Console.WriteLine("");
            PExemple3.Rectangle rectangleCarre3 = new PExemple3.Carre();
            Console.WriteLine("Rectangle = new Carre");
            rectangleCarre3.VoirDetails(); // Figure
            rectangleCarre3.VoirMonReference();



            // Traitement cercle (Figure)
            // Traitement Rectangle (Figure)

            // Traitement Cercle (c'est pas possible d'utiliser Rectangle )
        }
    }
}
