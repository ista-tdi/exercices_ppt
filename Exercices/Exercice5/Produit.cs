﻿using System;

namespace Exercice5
{
    class Produit
    {
        private string nom;
        private float prixAchat;
        private float prixVente;
        private int exemplaire;
        private string description;


        // Constructeur d'initialisation
        public Produit()
        {
            this.nom = "";
            this.prixAchat = 0;
            this.prixVente = 0;
            this.exemplaire = 0;
            this.description = "Vide";
        }

        // Constructeur d'initialisation
        public Produit(string nom, float prixAchat, float prixVente, int exemplaire, string description)
        {
            this.nom = nom;
            this.prixAchat = prixAchat;
            this.prixVente = prixVente;
            this.exemplaire = exemplaire;
            this.description = description;
        }


        // Constructeur de recopie
        public Produit(Produit produit)
        {
            this.nom = produit.nom;
            this.prixAchat = produit.prixAchat;
            this.prixVente = produit.prixVente;
            this.exemplaire = produit.exemplaire;
            this.description = produit.description;
        }


        public string Nom
        {
            get { return nom; }
            set { this.nom = value; }
        }

        public float PrixAchat
        {
            get { return prixAchat; }
            set { prixAchat = value; }
        }

        public float PrixVente
        {
            get { return prixVente; }
            set { prixVente = value; }
        }

        public int Exemplaire
        {
            get { return exemplaire; }
            set { exemplaire = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public void Afficher()
        {
            Console.WriteLine(" Nom: {0} -- PrixAchat: {1} --  PrixVente: {2} --  Exemplaire: {3} --  Description: {4} -- ",
                nom, prixAchat, prixVente, exemplaire, description);
        }
    }

}
