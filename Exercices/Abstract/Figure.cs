﻿using System;

namespace Abstract
{
    public abstract class Figure
    {
        public abstract double CalculePerimetre();
        public abstract double CalculeSurface();
    }

    public interface IFigure
    {
        public double CalculePerimetre();
        public double CalculeSurface();
    }

    public class Rectangle : Figure
    {
        private double longueur;
        private double largeur;


        public double Longueur
        {
            get { return longueur; }
            set { largeur = value; }
        }

        public double Largeur
        {
            get { return largeur; }
            set { largeur = value; }
        }


        public Rectangle(double longueur, double largeur)
        {
            this.longueur = longueur;
            this.largeur = largeur;
        }


        public override double CalculePerimetre()
        {
            return 2 * (longueur + largeur);
        }

        public override double CalculeSurface()
        {
            return longueur * largeur;
        }
    }

    public class Cercle : Figure
    {
        private double rayon;

        public double Rayon
        {
            get { return rayon; }
            set { rayon = value; }
        }

        public Cercle(double rayon) {
            this.rayon = rayon;
        }

        public override double CalculePerimetre()
        {
            return 2* Math.PI * rayon;
        }

        public override double CalculeSurface()
        {
            return Math.PI * rayon * rayon;
        }
    }
}
